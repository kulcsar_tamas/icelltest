package com.example.icelltest.mapper

import com.example.data.Mapper
import com.example.domain.Post
import com.example.icelltest.network.models.PostApiModel
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PostApiToDomainMapper
@Inject constructor() : Mapper<PostApiModel, Post> {

    override fun map(from: PostApiModel) = Post(
        id = from.id.toString(),
        title = from.title,
        body = from.body,
    )
}

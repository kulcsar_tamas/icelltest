package com.example.icelltest.mapper

import com.example.data.Mapper
import com.example.domain.User
import com.example.icelltest.network.models.UserApiModel
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserApiToDomainMapper
@Inject constructor() : Mapper<UserApiModel, User> {

    override fun map(from: UserApiModel): User {
        return User(
            id = from.id,
            name = from.name,
            username = from.username,
            email = from.email,
            phoneNumber = from.phoneNumber,
            city = from.address.city
        )
    }
}

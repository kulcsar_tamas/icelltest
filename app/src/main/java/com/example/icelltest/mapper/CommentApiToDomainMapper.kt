package com.example.icelltest.mapper

import com.example.data.Mapper
import com.example.domain.Comment
import com.example.icelltest.network.models.CommentApiModel
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CommentApiToDomainMapper
@Inject constructor() : Mapper<CommentApiModel, Comment> {

    override fun map(from: CommentApiModel) = Comment(
        id = from.id,
        name = from.name,
        body = from.body,
    )
}

package com.example.icelltest.action

import com.example.domain.Comment
import com.example.domain.Post
import com.example.domain.User
import com.example.icelltest.mapper.CommentApiToDomainMapper
import com.example.icelltest.mapper.PostApiToDomainMapper
import com.example.icelltest.mapper.UserApiToDomainMapper
import com.example.icelltest.network.ApiService
import dagger.hilt.android.scopes.ViewModelScoped
import javax.inject.Inject

@ViewModelScoped
class PostDetailsAction
@Inject constructor(
    private val apiService: ApiService,
    private val postApiToDomainMapper: PostApiToDomainMapper,
    private val commentApiToDomainMapper: CommentApiToDomainMapper,
    private val userAction: UserAction,
) {

    suspend fun getPost(id: String): Post {
        val postApiModel = apiService.getPost(id)
        return postApiModel?.let {
            val user = userAction.getUser(it.userId)
            postApiToDomainMapper.map(it).copy(
                user = user
            )

        } ?: throw Exception("No response!")
    }

    suspend fun getComments(postId: String): List<Comment> {
        val commentsApiModel = apiService.getCommentsForPost(postId)
        return commentsApiModel?.map {
            commentApiToDomainMapper.map(it)
        } ?: throw Exception("No response!")
    }
}

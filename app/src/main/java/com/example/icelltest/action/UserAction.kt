package com.example.icelltest.action

import com.example.domain.User
import com.example.icelltest.mapper.UserApiToDomainMapper
import com.example.icelltest.network.ApiService
import dagger.hilt.android.scopes.ViewModelScoped
import javax.inject.Inject

@ViewModelScoped
class UserAction
@Inject constructor(
    private val apiService: ApiService,
    private val userApiToDomainMapper: UserApiToDomainMapper,
) {

    suspend fun getUser(userId: String): User {
        val userApiModel = apiService.getUser(userId)
        return userApiModel?.let {
            userApiToDomainMapper.map(it)
        } ?: throw Exception("No response!")
    }
}

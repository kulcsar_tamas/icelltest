package com.example.icelltest.action

import androidx.compose.runtime.toMutableStateList
import com.example.domain.Post
import com.example.icelltest.mapper.PostApiToDomainMapper
import com.example.icelltest.network.ApiService
import dagger.hilt.android.scopes.ViewModelScoped
import javax.inject.Inject

@ViewModelScoped
class PostAction @Inject constructor(
    private val apiService: ApiService,
    private val postApiToDomainMapper: PostApiToDomainMapper,
) {

    suspend fun getPosts(): List<Post> {
        val posts = apiService.getPosts()
        return posts?.map {
            postApiToDomainMapper.map(it)
        }?.toMutableStateList() ?: throw Exception("No response!")
    }
}

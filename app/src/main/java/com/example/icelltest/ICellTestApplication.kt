package com.example.icelltest

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ICellTestApplication : Application()

package com.example.icelltest.features.userDetails

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.example.domain.User
import com.example.navigation.Route
import com.example.ui.components.DetailCard
import com.example.ui.theme.CombinedPreviews
import com.example.ui.theme.PreviewWrapper
import com.example.ui.theme.spacing

object UserDetailsRoute : Route<UserDetailsViewModel>("userDetails", "id") {

    fun withArg(id: String) = "${key}/$id"

    @Composable
    override fun viewModel(): UserDetailsViewModel = hiltViewModel()

    @Composable
    override fun Content(viewModel: UserDetailsViewModel) = UserDetailsScreen(viewModel)
}

@Composable
fun UserDetailsScreen(viewModel: UserDetailsViewModel) {
    val state by viewModel.screenState.collectAsStateWithLifecycle()
    UserDetailsScreen(state)
}

@Composable
private fun UserDetailsScreen(state: UserDetailsScreenState) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background),
        contentAlignment = Alignment.Center,
    ) {
        when (state) {
            is UserDetailsScreenState.Loading ->
                CircularProgressIndicator(
                    color = MaterialTheme.colorScheme.primary
                )

            is UserDetailsScreenState.Data ->
                LazyColumn(
                    modifier = Modifier.fillMaxSize(),
                    contentPadding = PaddingValues(MaterialTheme.spacing.small),
                    verticalArrangement = Arrangement.spacedBy(MaterialTheme.spacing.small)
                ) {
                    item {
                        DetailCard(title = "Name", value = state.user.name)
                    }
                    item {
                        DetailCard(title = "Username", value = state.user.username)
                    }
                    item {
                        DetailCard(title = "Email", value = state.user.email)
                    }
                    item {
                        DetailCard(title = "Phone number", value = state.user.phoneNumber)
                    }
                    item {
                        DetailCard(title = "City", value = state.user.city)
                    }
                }

            is UserDetailsScreenState.Error ->
                Text(text = "Something went wrong", color = MaterialTheme.colorScheme.error)
        }
    }
}

@CombinedPreviews
@Composable
private fun UserDetailsScreenPreview() {
    PreviewWrapper {
        UserDetailsScreen(
            state = UserDetailsScreenState.Data(
                user = User("1", "name", "username", "email", "phone", "city")
            )
        )
    }
}

package com.example.icelltest.features.posts

import androidx.compose.runtime.toMutableStateList
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.icelltest.action.PostAction
import com.example.icelltest.features.postDetails.PostDetailsRoute
import com.example.navigation.Navigator
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

@HiltViewModel
class PostsViewModel
@Inject constructor(
    navigator: Navigator,
    postAction: PostAction,
) : ViewModel(), Navigator by navigator {

    val screenState = MutableStateFlow<PostsScreenState>(PostsScreenState.Loading)

    init {
        viewModelScope.launch {
            runCatching {
                screenState.emit(PostsScreenState.Loading)
                postAction.getPosts()
            }.onSuccess {
                screenState.emit(
                    PostsScreenState.Data(
                        posts = it.toMutableStateList()
                    )
                )
            }.onFailure {
                screenState.emit(PostsScreenState.Error)
            }
        }
    }

    fun onItemClicked(item: String) {
        viewModelScope.launch {
            navigate(PostDetailsRoute.withArg(item))
        }
    }
}

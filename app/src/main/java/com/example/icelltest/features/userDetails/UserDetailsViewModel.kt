package com.example.icelltest.features.userDetails

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.icelltest.action.UserAction
import com.example.navigation.Navigator
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

@HiltViewModel
class UserDetailsViewModel @Inject constructor(
    navigator: Navigator,
    savedStateHandle: SavedStateHandle,
    userAction: UserAction,
) : ViewModel(), Navigator by navigator {

    val id = savedStateHandle.get<String>(
        UserDetailsRoute.arg ?: error("No arg value present!")
    ).orEmpty()

    val screenState = MutableStateFlow<UserDetailsScreenState>(
        UserDetailsScreenState.Loading
    )

    init {
        viewModelScope.launch {
            runCatching {
                userAction.getUser(id)
            }.onSuccess {
                screenState.emit(
                    UserDetailsScreenState.Data(
                        user = it
                    )
                )
            }.onFailure {
                screenState.emit(
                    UserDetailsScreenState.Error
                )
            }
        }
    }
}

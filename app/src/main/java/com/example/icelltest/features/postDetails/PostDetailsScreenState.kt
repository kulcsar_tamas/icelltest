package com.example.icelltest.features.postDetails

import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.snapshots.SnapshotStateList
import com.example.domain.Comment
import com.example.domain.Post
import kotlinx.coroutines.flow.MutableStateFlow

sealed interface PostDetailsScreenState {
    data class Data(
        val post: MutableStateFlow<Post?> = MutableStateFlow(null),
        val comments: SnapshotStateList<Comment> = mutableStateListOf(),
    ) : PostDetailsScreenState

    object Error : PostDetailsScreenState
}

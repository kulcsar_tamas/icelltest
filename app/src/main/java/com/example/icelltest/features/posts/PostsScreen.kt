package com.example.icelltest.features.posts

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.domain.Post
import com.example.navigation.Route
import com.example.ui.components.ListItem
import com.example.ui.theme.CombinedPreviews
import com.example.ui.theme.PreviewWrapper
import com.example.ui.theme.spacing

object PostsRoute : Route<PostsViewModel>("posts") {

    @Composable
    override fun viewModel(): PostsViewModel = hiltViewModel()

    @Composable
    override fun Content(viewModel: PostsViewModel) = PostsScreen(viewModel = viewModel)
}

@Composable
fun PostsScreen(viewModel: PostsViewModel) {
    val state: PostsScreenState by viewModel.screenState.collectAsState()
    PostsScreen(
        onItemClicked = viewModel::onItemClicked,
        state = state
    )
}

@Composable
private fun PostsScreen(onItemClicked: (String) -> Unit, state: PostsScreenState) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background),
        contentAlignment = Alignment.Center,
    ) {
        when (state) {
            is PostsScreenState.Data -> {
                val posts = state.posts
                LazyColumn(
                    contentPadding = PaddingValues(MaterialTheme.spacing.small),
                    verticalArrangement = Arrangement.spacedBy(
                        MaterialTheme.spacing.small
                    ),

                    ) {
                    items(posts, key = { it.id }) {
                        ListItem(
                            title = it.title,
                            body = it.body,
                            onClick = {
                                onItemClicked(it.id)
                            },
                        )
                    }
                }
            }

            is PostsScreenState.Loading ->
                CircularProgressIndicator(
                    color = MaterialTheme.colorScheme.primary
                )


            is PostsScreenState.Error ->
                Text(text = "Something went wrong", color = MaterialTheme.colorScheme.error)
        }
    }
}

@CombinedPreviews
@Composable
private fun PostsScreenPreview() {
    PreviewWrapper {
        PostsScreen(
            onItemClicked = { }, PostsScreenState.Data(
                remember {
                    mutableStateListOf(
                        Post("1", "fsrf", "gsrgdtgdtghd"),
                        Post("1", "fsrf", "gsrgdtgdtghd"),
                        Post("1", "fsrf", "gsrgdtgdtghd"),
                    )
                }
            )
        )
    }
}

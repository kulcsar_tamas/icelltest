package com.example.icelltest.features.posts

import androidx.compose.runtime.snapshots.SnapshotStateList
import com.example.domain.Post
import kotlinx.coroutines.flow.Flow

sealed interface PostsScreenState {

    object Loading : PostsScreenState
    data class Data(
        val posts: SnapshotStateList<Post>,
    ) : PostsScreenState

    object Error : PostsScreenState
}

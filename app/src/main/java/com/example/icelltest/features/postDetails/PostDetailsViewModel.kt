package com.example.icelltest.features.postDetails

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.icelltest.action.PostDetailsAction
import com.example.icelltest.features.userDetails.UserDetailsRoute
import com.example.navigation.Navigator
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch

@HiltViewModel
class PostDetailsViewModel
@Inject constructor(
    navigator: Navigator,
    savedStateHandle: SavedStateHandle,
    postDetailsAction: PostDetailsAction,
) : ViewModel(), Navigator by navigator {

    private val id = savedStateHandle.get<String>(
        PostDetailsRoute.arg ?: error("No arg value present!")
    ).orEmpty()

    private val dataState = PostDetailsScreenState.Data()
    val screenState = MutableStateFlow<PostDetailsScreenState>(dataState)

    val onUserClicked: () -> Unit = {
        viewModelScope.launch {
            val userId = dataState.post.first()?.user?.id ?: error("No user id!")
            navigate(UserDetailsRoute.withArg(userId))
        }
    }

    init {
        viewModelScope.launch {
            runCatching {
                postDetailsAction.getPost(id)
            }.onSuccess {
                dataState.post.emit(it)
                screenState.emit(dataState)
            }.onFailure {
                screenState.emit(PostDetailsScreenState.Error)
            }
        }

        viewModelScope.launch {
            runCatching {
                postDetailsAction.getComments(id)
            }.onSuccess {
                dataState.comments.apply {
                    clear()
                    addAll(it)
                }
                screenState.emit(dataState)
            }.onFailure {
                screenState.emit(PostDetailsScreenState.Error)
            }
        }
    }
}

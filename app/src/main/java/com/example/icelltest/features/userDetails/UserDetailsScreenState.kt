package com.example.icelltest.features.userDetails

import com.example.domain.User

sealed interface UserDetailsScreenState {

    object Loading : UserDetailsScreenState
    data class Data(
        val user: User,
    ) : UserDetailsScreenState

    object Error : UserDetailsScreenState
}

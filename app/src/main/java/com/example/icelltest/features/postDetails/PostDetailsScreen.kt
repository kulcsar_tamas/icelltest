package com.example.icelltest.features.postDetails

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.example.domain.Comment
import com.example.domain.Post
import com.example.domain.User
import com.example.navigation.Route
import com.example.ui.components.ListItem
import com.example.ui.theme.CombinedPreviews
import com.example.ui.theme.ICellTestTheme
import com.example.ui.theme.spacing
import kotlinx.coroutines.flow.MutableStateFlow

object PostDetailsRoute : Route<PostDetailsViewModel>("postDetails", "id") {

    fun withArg(id: String) = "$key/$id"

    @Composable
    override fun viewModel(): PostDetailsViewModel = hiltViewModel()

    @Composable
    override fun Content(viewModel: PostDetailsViewModel) = PostDetailsScreen(viewModel)
}

@Composable
fun PostDetailsScreen(viewModel: PostDetailsViewModel) {
    val state by viewModel.screenState.collectAsStateWithLifecycle()
    PostDetailsScreen(state = state, onUserClicked = viewModel.onUserClicked)
}

@Composable
fun PostDetailsScreen(state: PostDetailsScreenState, onUserClicked: () -> Unit = {}) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background),
        contentAlignment = Alignment.Center,
    ) {
        when (state) {
            is PostDetailsScreenState.Data -> {
                val post by state.post.collectAsState()
                val comments = state.comments
                LazyColumn(
                    modifier = Modifier
                        .fillMaxSize(),
                    horizontalAlignment = Alignment.Start,
                    verticalArrangement = Arrangement.spacedBy(
                        MaterialTheme.spacing.small,
                    ),
                    contentPadding = PaddingValues(MaterialTheme.spacing.small)
                ) {
                    item {
                        AnimatedVisibility(visible = post != null) {
                            Column {
                                Text(
                                    text = "Post ${post?.id} details",
                                    style = MaterialTheme.typography.headlineMedium,
                                    color = MaterialTheme.colorScheme.primary
                                )
                                Spacer(modifier = Modifier.height(MaterialTheme.spacing.small))
                                Text(
                                    text = post?.title.orEmpty(),
                                    style = MaterialTheme.typography.bodyLarge,
                                    color = MaterialTheme.colorScheme.primary
                                )
                                Spacer(modifier = Modifier.height(MaterialTheme.spacing.small))
                                Text(
                                    text = post?.body.orEmpty(),
                                    style = MaterialTheme.typography.bodyMedium,
                                    color = MaterialTheme.colorScheme.primary
                                )
                                Spacer(modifier = Modifier.height(MaterialTheme.spacing.roomy))
                                AnimatedVisibility(visible = post?.user != null) {
                                    Button(onClick = onUserClicked) {
                                        Text(
                                            text = post?.user?.name.orEmpty(),
                                            style = MaterialTheme.typography.bodyLarge,
                                            color = MaterialTheme.colorScheme.onPrimary,
                                        )
                                    }
                                }
                            }
                        }
                    }
                    items(items = comments, key = { it.id }) {
                        ListItem(
                            title = it.name,
                            body = it.body,
                            onClick = {}
                        )
                    }
                }
            }

            is PostDetailsScreenState.Error ->
                Text(text = "Something went wrong", color = MaterialTheme.colorScheme.error)
        }
    }
}

@CombinedPreviews
@Composable
private fun PostDetailsScreenPreview() {
    ICellTestTheme {
        PostDetailsScreen(
            state = PostDetailsScreenState.Data(
                post = MutableStateFlow(
                    Post(
                        "", "sfrg jsgdth", "sirgsgurg", user = User(
                            "", "fsrf", "", "", "", ""
                        )
                    )
                ),
                comments = remember {
                    mutableStateListOf(
                        Comment(1, "sorgs", "srgddtgokaef"),
                        Comment(2, "sorgs", "srgddtgokaef"),
                        Comment(3, "sorgs", "srgddtgokaef"),
                        Comment(4, "sorgs", "srgddtgokaef"),
                    )
                }
            )
        )
    }
}

package com.example.icelltest.network.models

import com.google.gson.annotations.SerializedName

data class CommentApiModel(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("body")
    val body: String,
)

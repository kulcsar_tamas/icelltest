package com.example.icelltest.network

import com.example.icelltest.network.models.CommentApiModel
import com.example.icelltest.network.models.PostApiModel
import com.example.icelltest.network.models.UserApiModel
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {

    @GET("posts")
    suspend fun getPosts(): List<PostApiModel>?

    @GET("posts/{id}")
    suspend fun getPost(@Path("id") id: String): PostApiModel?

    @GET("posts/{id}/comments")
    suspend fun getCommentsForPost(@Path("id") id: String): List<CommentApiModel>?

    @GET("users/{id}")
    suspend fun getUser(@Path("id") id: String): UserApiModel?
}

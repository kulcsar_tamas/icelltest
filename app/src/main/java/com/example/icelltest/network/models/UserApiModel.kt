package com.example.icelltest.network.models

import com.google.gson.annotations.SerializedName

data class UserApiModel(
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("username")
    val username: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("phone")
    val phoneNumber: String,
    @SerializedName("address")
    val address: Address
) {

    data class Address(
        @SerializedName("city")
        val city: String,
    )
}

package com.example.icelltest

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.rememberNavController
import com.example.icelltest.features.postDetails.PostDetailsRoute
import com.example.icelltest.features.posts.PostsRoute
import com.example.icelltest.features.userDetails.UserDetailsRoute
import com.example.ui.theme.ICellTestTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ICellTestTheme {
                val navController = rememberNavController()
                NavHost(navController = navController, startDestination = PostsRoute.route) {
                    PostsRoute.screen(this, navController)
                    PostDetailsRoute.screen(this, navController)
                    UserDetailsRoute.screen(this, navController)
                }
            }
        }
    }
}

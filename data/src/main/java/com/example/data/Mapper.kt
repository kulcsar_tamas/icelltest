package com.example.data

interface Mapper<F, T> {

    fun map(from: F): T
}

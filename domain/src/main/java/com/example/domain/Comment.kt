package com.example.domain

data class Comment(
    val id: Int,
    val name: String,
    val body: String,
)

package com.example.domain

data class User(
    val id: String,
    val name: String,
    val username: String,
    val email: String,
    val phoneNumber: String,
    val city: String,
)

package com.example.navigation

import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel

interface Navigator {
    val navigationReceiver: ReceiveChannel<String>
    suspend fun navigate(destination: String)
}

class NavigatorImpl : Navigator {

    private val navigationChannel = Channel<String>()
    override val navigationReceiver: ReceiveChannel<String> = navigationChannel

    override suspend fun navigate(destination: String) {
        navigationChannel.send(destination)
    }
}

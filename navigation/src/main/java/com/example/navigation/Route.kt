package com.example.navigation

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.composable

abstract class Route<T : Navigator>(
    protected val key: String,
    val arg: String? = null,
) {

    val route = "$key${arg?.let { "/{$it}" }.orEmpty()}"

    @Composable
    abstract fun Content(viewModel: T)

    @Composable
    abstract fun viewModel(): T

    fun screen(
        navGraphBuilder: NavGraphBuilder,
        navHostController: NavHostController,
    ) {
        navGraphBuilder.composable(this@Route.route) {
            val viewModel = viewModel()
            LaunchedEffect(navHostController) {
                for (destination in viewModel.navigationReceiver) {
                    navHostController.navigate(destination)
                }
            }
            Content(viewModel)
        }
    }
}
